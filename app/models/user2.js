
import Model from "./model";

class User2 extends Model {
   constructor(table="rm_users", configs={"database": "otherdb"}){
      super(table, configs);
      this.table = table;
      this.configs = configs;
   }

}

export default User2
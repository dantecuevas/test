
import db from "../configs/database";
import mysql from 'mysql';
/*
   let filter = {
      where : [{
         field : "id_company",
         value : 2
      }, {
         field : "user",
         value : "test"
      }, {
         field : "price",
         value : 2.45,
         compare: ">="
      }]
   };
*/
class Model {
   constructor (table, configs={"database": "default"}){
      this.table = table;
      this.configs = configs;

      this.fields = "*";
      this.where = "";
      this.quote = "'";
      this.compare = "=";

      this.orderBy = "";
      this.order = "ASC";

      this.limit = "";
      console.log("CONTRUCTOR MODEL");
      console.log("DB: " + this.configs.database);
      console.log("TABLA: " + this.table);
      console.log("*****************************************");
      this.connection = mysql.createConnection({
         host: db[this.configs.database].host,
         port: db[this.configs.database].port,
         user: db[this.configs.database].user,
         password: db[this.configs.database].password,
         database: db[this.configs.database].database
      });
   }

   find (filter, callback) {
      if(this.connection){
         this.where = "";
         this.orderBy = "";
         this.limit = "";
         if(filter !== null) {
            for(var key in filter.where){
               if(key == 0) {
                  this.where = " WHERE ";
               } else {
                  this.where += " AND ";
               }
               if(typeof(filter.where[key].value) == "number"){
                  this.quote = "";
               }
               if(filter.where[key].compare){
                  this.compare = filter.where[key].compare;
               }
               this.where += filter.where[key].field + this.compare + this.quote + filter.where[key].value + this.quote;
               this.quote = "'";
            };
            for(var key in filter.orderBy){
               if(key == 0) {
                  this.orderBy = " ORDER BY ";
               } else {
                  this.orderBy += ", ";
               }
               if(filter.orderBy[key].order){
                  this.order = filter.orderBy[key].order;
               }
               this.orderBy += filter.orderBy[key].field +" "+ this.order;
            };

            if(filter.limit){
               this.limit = " LIMIT "+ filter.limit.start +", "+ filter.limit.end;
            }
         }
         let sql = "SELECT "+this.fields+" FROM "+this.table+this.where+this.orderBy+this.limit;
         console.log(sql);
         this.connection.query(sql, function (error, rows) {
            if(error){
               throw error;
            } else {
               callback(null, rows);
            }
         });
      }
   }

   insert (data, callback) {
      if(this.connection){
         let sql = "INSERT INTO "+this.table+" SET ?";
         console.log(sql);
         this.connection.query(sql, data, function (error, result) {
            if(error){
               throw error;
            } else {
               callback(null, {"insertId" : result.insertId});
            }
         });
      }
   }

   update (fields, filter, callback) {
      if(this.connection){
         let data = [fields, filter];
         let sql = "UPDATE "+this.table+" SET ? WHERE ?";
         console.log(sql);
         this.connection.query(sql, data, function (error, result) {
            if(error){
               throw error;
            } else {
               callback(null, {"msg" : "success"});
            }
         });
      }
   }

   delete (id, callback) {
      if(this.connection){
         let sql = "SELECT * FROM "+this.table+" WHERE id = " + this.connection.escape(id);
         this.connection.query (sql, function (error, row) {
            if(row) {
               sql = "DELETE FROM " +this.table+ " WHERE id=" +this.connection.escape(id);
               this.connection.query(sql, function (err, result) {
                  if(err){
                     throw err;
                  } else {
                     callback(null, {"msg" : "deleted"});
                  }
               });
            } else {
               callback(null, {"msg" : "notExist"});
            }
         });
      }
   }

}

export default Model
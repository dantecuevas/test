
import Model from "./model";

class User extends Model {
   constructor(table="user"){
      super(table);
      this.table = table;
   }

}

export default User
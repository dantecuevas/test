
/*import md5 from 'md5';
import moment from 'moment';*/
import helper from "../configs/helper";
import moment from "moment";
import nodemailer from 'nodemailer';

/*import UserModel from "../models/user";
let User = new UserModel();*/
import VoucherModel from "../models/voucher";
let Voucher = new VoucherModel();

import Change2Model from "../models/change2";
let Change2 = new Change2Model();

let smtpTransport =  nodemailer.createTransport('smtps://mgmegalinea00%40gmail.com:a123456aa@smtp.gmail.com');

let clientController = function (app, control={auth, passport, acl}){

   function controller (req, res, next) {
      req.controller = "client";
      return next();
   }

   app.get('/client', control.auth, (req, res) => {

      let userName = "login";
      let filter = {
         where : [{
            field : "company_id",
            value : req.user.company_id
         }, {
            field : "user_username",
            value : req.user.username
         }],
         orderBy : [{
            field : "created",
            order : "DESC"
         }],
         limit : {
            start : 0,
            end : 4
         }
      };
      if(req.user){
         userName = req.user.username;
      }

      Voucher.find(filter, function (err, rows) {
         if (typeof rows !== 'undefined') {
            res.render('client/voucher', {userName: userName, vouchers: rows});
         } else {
            res.send({
               result : 'error',
               err : err.code
            });
         }
      });
      
   });

   app.get('/clientvoucheradd', control.auth, (req, res) => {

      let userName = "";
      let filter = {
         where : [{
            field : "username",
            value : req.user.username
         }]
      };
      if(req.user){
         userName = req.user.username;
      }
      console.log(helper.bancos);
      //res.render('client/voucheradd', {userName: userName, bancos: helper.bancos});
      Change2.find(filter, function (err, rows) {
         if (typeof rows !== 'undefined') {
            let newsrvname = rows[0].newsrvname;
               if(newsrvname.substring(0, 2) == "  ")
                  newsrvname = newsrvname.substring(2);
            let montoPay = newsrvname.split(" ")[1].substring(1);
            console.log("pay: " +  montoPay);
            let dateA = moment(req.user.expiration);
            let dateB = moment();
            console.log("A :"+dateA);
            console.log("B :"+dateB);

            console.log('Difference is - ', dateA.diff(dateB, 'days'), 'days');
            console.log('Difference is * ', dateB.diff(dateA, 'days'), 'days');
            res.render('client/voucheradd', {userName: userName, expiration: req.user.expiration, montoPay: montoPay, bancos: helper.bancos, periodos: helper.periodos});
         } else {
            res.send({
               result : 'error',
               err : err.code
            });
         }
      });
      
   });

   app.post('/clientvoucher/add', control.auth, (req, res) => {
      console.log(req.body);
      let date = moment().format('YYYY-MM-DD H:mm:ss');
      let $d = req.body;
      let data = {
         id : null,
         banco : $d.banco,
         fecha : $d.fecha,
         noperacion : $d.noperacion,
         monto : $d.monto,
         celular : $d.celular,
         imagen : $d.imagen || "",
         observacion : $d.observacion,
         user_username : req.user.username,
         company_id : req.user.company_id,
         created : date,
         createdby : req.user.id,
         updated : date,
         updatedby : req.user.id
      };

      Voucher.insert(data, function (err, result) {
           //si el usuario se ha insertado correctamente mostramos su info
         if(result && result.insertId) {
            console.log('success');
            let html = "";
               html += "<br>Usuario: "+data.user_username+"</br>";
               html += "<br>Banco: "+data.banco+"</br>";
               html += "<br>Nro Operacion: "+data.noperacion+"</br>";
               html += "<br>Monto: "+data.monto+"</br>";
               html += "<br>Fecha y hora: "+data.fecha+"</br>";
               html += "<br>Celular: "+data.celular+"</br>";
               html += "<br>Observacion: "+data.observacion+"</br>";
            let mailOptions = {
               from: "MEGALINEA<mgmegalinea00@gmail.com>",
               to: "mgmegalinea@gmail.com",
               //to: "elexfima.2.dante@gmail.com",
               subject: "Registro de voucher",
               html: html
            };

            /*smtpTransport.sendMail(mailOptions, function(error, response){
               if(error){
                  console.log("No Send");
                  console.log(error);
               }else{
                  console.log("Send");
               }
            });*/

            res.redirect('/client');
         } else {
            console.log(err);
            res.statusCode = 500;
            res.send({
               result : 'error',
               err : err.code
            });
         }
      });
   });

   app.get('/clientvoucheredit:id', control.auth, (req, res) => {
      console.log("id: "+req.params.id);

      let userName = "";
      let filter = {
         where : [{
            field : "username",
            value : req.user.username
         }]
      };
      if(req.user){
         userName = req.user.username;
      }
      console.log(helper.bancos);
      Change2.find(filter, function (err, rows) {
         if (!err) {
            let newsrvname = rows[0].newsrvname;
               if(newsrvname.substring(0, 2) == "  ")
                  newsrvname = newsrvname.substring(2);
            let montoPay = newsrvname.split(" ")[1].substring(1);
            console.log("pay: " +  montoPay);
            let dateA = moment(req.user.expiration);
            let dateB = moment();
            console.log("A :"+dateA);
            console.log("B :"+dateB);

            console.log('Difference is - ', dateA.diff(dateB, 'days'), 'days');
            console.log('Difference is * ', dateB.diff(dateA, 'days'), 'days');

            filter = {
               where : [{
                  field : "company_id",
                  value : req.user.company_id
               }, {
                  field : "user_username",
                  value : req.user.username
               }, {
                  field : "id",
                  value : req.params.id
               }, {
                  field : "validado",
                  value : 1
               }]
            };
            Voucher.find(filter, function (err2, rows2) {
               console.log("llego");
               if (!err2) {
                  if(rows2.length)
                     return res.render('client/voucheredit', {userName: userName, expiration: req.user.expiration, montoPay: montoPay, bancos: helper.bancos, periodos: helper.periodos, voucher: rows2[0]});
                  res.send({
                     result : 'error',
                     err : "no data"
                  });
               } else {
                  console.log(err2);
                  res.send({
                     result : 'error',
                     err : err2.code
                  });
               }
            });
         } else {
            res.send({
               result : 'error',
               err : err.code
            });
         }
      });      
   });

   app.post('/clientvoucher/edit', control.auth, (req, res) => {
      console.log(req.body);
      let date = moment().format('YYYY-MM-DD H:mm:ss');
      let $d = req.body;
      let data = {
         banco : $d.banco,
         fecha : $d.fecha,
         noperacion : $d.noperacion,
         monto : $d.monto,
         celular : $d.celular,
         imagen : $d.imagen || "",
         observacion : $d.observacion,
         validado : 0,
         intento : parseInt($d.intento, 10) + 1,
         updated : date,
         updatedby : req.user.id
      };
      let filter = {
         id : $d.id
      };

      Voucher.update(data, filter, function (err, result) {
           //si el usuario se ha insertado correctamente mostramos su info
         if(result && result.msg) {
            console.log('success');
            let html = "";
               html += "<br>Voucher actualizado";
               html += "<br>Usuario: "+data.user_username+"</br>";
               html += "<br>Banco: "+data.banco+"</br>";
               html += "<br>Nro Operacion: "+data.noperacion+"</br>";
               html += "<br>Monto: "+data.monto+"</br>";
               html += "<br>Fecha y hora: "+data.fecha+"</br>";
               html += "<br>Celular: "+data.celular+"</br>";
               html += "<br>Observacion: "+data.observacion+"</br>";
            let mailOptions = {
               from: "MEGALINEA<danieldantecuevas@gmail.com>",
               //to: "mgmegalinea@gmail.com",
               to: "elexfima.2.dante@gmail.com",
               subject: "Registro de voucher",
               html: html
            };

            /*smtpTransport.sendMail(mailOptions, function(error, response){
               if(error){
                  console.log("No Send");
                  console.log(error);
               }else{
                  console.log("Send");
               }
            });*/

            res.redirect('/client');
         } else {
            console.log(err);
            res.statusCode = 500;
            res.send({
               result : 'error',
               err : err.code
            });
         }
      });
   });

}

export default clientController

import VoucherModel from "../models/voucher";
let Voucher = new VoucherModel();

let voucherController = function (app, control={auth, passport, acl}){

   function controller (req, res, next) {
      req.controller = "voucher";
      return next();
   }

   app.get('/voucher', [control.auth, controller, control.acl], (req, res) => {
      let userName = "login";
      let filter = {
         where : [{
            field : "company_id",
            value : 1
         }],
         orderBy : [{
            field : "created",
            order : "DESC"
         }]
      };
      if(req.user){
         userName = req.user.username;
      }

      Voucher.find(filter, function (err, rows) {
         if (typeof rows !== 'undefined') {
            res.render('voucher/index', {userName: userName, vouchers: rows});
         } else {
            res.send({
               result : 'error',
               err : err.code
            });
         }
      });
   });

   app.get('/voucheradd', [control.auth, controller, control.acl], (req, res) => {

      let userName = "";
      let filter = {
         where : [{
            field : "company_id",
            value : req.user.company_id
         }]
      };
      if(req.user){
         userName = req.user.username;
      }

      res.render('voucher/add', {userName: userName});

      /*User.find(filter, function (err, rows) {
         if (typeof rows !== 'undefined') {
            res.render('user/add', {userName: userName, vouchers: rows});
         } else {
            res.send({
               result : 'error',
               err : err.code
            });
         }
      });*/
      
   });

   app.post('/voucher/add', [control.auth, controller, control.acl], (req, res) => {
      console.log(req.body);
      let $data = req.body;
      let data = {
         id : null,
         user_id : req.user.id,
         banco : req.body.Banco,
         monto : 12.3,
         id_company : 1
      };

      Voucher.insert(data, function (err, result) {
           //si el usuario se ha insertado correctamente mostramos su info
         if(result && result.insertId) {
            console.log('success');
            res.redirect('/');
         } else {
            console.log(err);
            res.statusCode = 500;
            res.send({
               result : 'error',
               err : err.code
            });
         }
      });
   });

   app.post('/voucher/validate', [control.auth, controller, control.acl], (req, res) => {
      console.log(req.body);

      let fields  = {validado: req.body.validado};
      let condition = {id: req.body.id};
      Voucher.update(fields, condition, function (err, data) {
           //si el usuario se ha insertado correctamente mostramos su info
         if(data && data.msg) {
            console.log('success');
            res.redirect('/voucher');
         } else {
            console.log(err);
         }
      });
   });

}

export default voucherController

import md5 from 'md5';
import moment from 'moment';

import UserModel from "../models/user";
let User = new UserModel();
/*import VoucherModel from "../models/voucher";
let Voucher = new VoucherModel();*/

let userController = function (app, control={auth, passport, acl}){

   function controller (req, res, next) {
      req.controller = "user";
      return next();
   }

   app.get('/login', (req, res) => {
      res.render('user/login');
   });

   app.post('/login', (req, res, next) => {
      control.passport.authenticate('local', (err, user) => {
      switch (req.accepts('html', 'json')) {
         case 'html':
            if (err) { return next(err); }
            if (!user) { return res.redirect('/login'); }
            console.log(user);
            req.logIn(user, function (err) {
               if (err) { return next(err); }
               if(user.type == "adm") {
                  return res.redirect('/admin');
               }else if(user.type == "cli") {
                  return res.redirect('/client');
               }               
            });
            break;
         case 'json':
            if (err)  { return next(err); }
            if (!user) { return res.status(401).send({"login": false}); }
            req.logIn(user, function (err) {
               if (err) { return res.status(401).send({"login": false}); }
               return res.send({"login": true, "username": user.username});
            });
            break;
         default:
            res.status(406).send();
      }

      })(req, res, next);
      
   });
   
   app.get('/logout', (req, res) => {
      req.logout();
      res.redirect('/');
   });

   app.get('/admin', [control.auth, controller, control.acl], (req, res) => {

      let userName = "login";
      let filter = {
         where : [{
            field : "company_id",
            value : 1
         }]
      };
      if(req.user){
         userName = req.user.username;
      }

      res.render('user/admin', {userName: userName});

      /*Voucher.find(filter, function (err, rows) {
         if (typeof rows !== 'undefined') {
            res.render('user/admin', {userName: userName, vouchers: rows});
         } else {
            res.send({
               result : 'error',
               err : err.code
            });
         }
      });*/
      
   });

   app.get('/user', [control.auth, controller, control.acl], (req, res) => {
      let userName = "login";
      let filter = {
         where : [{
            field : "company_id",
            value : 1
         }]
      };
      if(req.user){
         userName = req.user.username;
      }
      User.find(filter, function (err, rows) {
         if (typeof rows !== 'undefined') {
            res.render('user/index', {userName: userName, users: rows});
         } else {
            res.send({
               result : 'error',
               err : err.code
            });
         }
      });

   });

   app.get('/useradd', [control.auth, controller, control.acl], (req, res) => {

      let userName = "";
      let filter = {
         where : [{
            field : "company_id",
            value : req.user.company_id
         }]
      };
      if(req.user){
         userName = req.user.username;
      }

      res.render('user/add', {userName: userName});
      
   });

   app.post('/user/add', [control.auth, controller, control.acl], (req, res) => {

      let userName = "";
      let date = moment().format('YYYY-MM-DD H:mm:ss');
      let data = {
         id: null,
         username: req.body.username,
         password: md5(req.body.password),
         nombre: req.body.nombre,
         apellido: req.body.apellido,
         celular: req.body.celular,
         company_id: req.user.company_id,
         created: date,
         createdby: req.user.id,
         updated: date,
         updatedby: req.user.id,
      };
      if(req.user){
         userName = req.user.username;
      }
      console.log(data);

      User.insert(data, function (err, result) {
         if (result && result.insertId) {
            //res.render('useradd', {userName: userName, vouchers: rows});
            return res.redirect('/user');
         } else {
            res.send({
               result : 'error',
               err : err.code
            });
         }
      });      
   });
}

export default userController
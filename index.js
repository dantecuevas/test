
import express from 'express';
import http from 'http';

import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import expressSession from 'express-session';
import passport from 'passport';

import {Strategy as LocalStrategy} from 'passport-local';

const port = process.env.PORT || 3000;
const app = express();

import UserModel from './app/models/user';
import UserModel2 from './app/models/user2';

import md5 from 'md5';

app.set('views', __dirname + '/app/views');
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(expressSession({
   secret: 'SECRET-KEY',
   resave: false,
   saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

app.use('/', express.static(__dirname + '/public'));

let localStrategy = new LocalStrategy( (username, password, done) => {

   //Logica de BASE DE DATOS   
   let filter = {
      where : [{
         field : "username",
         value : username.substring(3)
      }, {
         field : "password",
         value : md5(password)
      }]
   };

   if(username.substring(0,3) == "cli") {
      let User2 = new UserModel2();
      User2.find(filter, function (err, rows) {
         if(err) {
            done(null, false, {
               message: 'Error'
            });
         }
         if(rows.length) {
            return done(null, {
               id: 0,
               username: rows[0].username,
               expiration: rows[0].expiration,
               type: "cli",
               rol: 0,
               company_id : 1
            });
         }else {
            done(null, false, {
               message: 'Unkown user'
            });
         }      
      });

   } else {
      let User = new UserModel();
      User.find(filter, function (err, rows) {
         if(err) {
            done(null, false, {
               message: 'Error'
            });
         }
         if(rows.length) {
            return done(null, {
               id: rows[0].id,
               username: rows[0].username,
               type: "adm",
               rol: 1,
               company_id : rows[0].company_id
            });
         }else {
            done(null, false, {
               message: 'Unkown user'
            });
         }      
      });

   }   
   
});

passport.use(localStrategy);
passport.serializeUser((user, done) => { done(null, user) });
passport.deserializeUser((user, done) => { done(null, user) });


import voucherController from './app/controllers/voucher';
voucherController(app, {auth: ensureAuth, acl: ensureACL});

import userController from './app/controllers/user';
userController(app, {passport: passport, auth: ensureAuth, acl: ensureACL});

import clientController from './app/controllers/client';
clientController(app, {auth: ensureAuth, acl: ensureACL});

import homeController from './app/controllers/home';
homeController(app, {auth: ensureAuth});

function ensureAuth (req, res, next){
   if(req.isAuthenticated()) {
      return next();
   }
   console.log("LOGIN NO");
   return res.status(401).send({"login": false});
}

import acl from './app/configs/acl';
function ensureACL (req, res, next){
   for(let key in acl.controller[req.controller].endpoint){
      if(acl.controller[req.controller].endpoint[key].uri == req.url){
         let rol = acl.controller[req.controller].endpoint[key].rol;
         if(rol.indexOf(req.user.rol) > -1)
            return next();
      }
   }
   return res.status(401).send({"privilege": false});
}


let server = http.createServer(app).listen(port, () => {
   console.log(`El servidor esta levantado en el puerto ${port}`);
});
